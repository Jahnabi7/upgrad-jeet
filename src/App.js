import React from 'react';
import './App.css';
import Navbar from "./components/Navbar";
import Banner from "./components/Banner";
import Courses from "./components/Courses";
import Testimonials from "./components/Testimonials";
import Footer from "./components/Footer";

function App() {
  return (
    <div className="App">
      <Navbar/>
      <Banner/>
      <Courses/>
      <Testimonials/>
      <Footer/>
    </div>
  );
}

export default App;
