import { nodeName } from "jquery";
import React, { Component } from "react";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Div: {
        showDiv: true,
      },
    };
  }
  handleClose = () => {
    this.setState({ showDiv: false });
  };
  render() {
    return (
      <>
        <div className="container">
          <div
            className="container header"
            style={{ display: this.state.showDiv ? "none" : "block" }}
          >
            India's first ever UGC recognized online BBA, BCA, MBA degree from
            Chandigarh University.&nbsp;&nbsp;
            <a href="#" className="btn btn-sm mt-2 mb-2" >
              KNOW MORE
            </a>
            <button
              onClick={this.handleClose}
              type="button"
              className="close"
              aria-label="Close"
            >
              <span  aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
        <nav className="navbar navbar-expand-lg navbar-light">
          <div className="container navbar">
            <a className="navbar-brand">
              <img
                src="./images/up_logo.png"
                height="40"
                alt="logo"
                className="d-inline-block"
              />
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#Navbar"
              aria-controls="Navbar"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="Navbar">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <a className="nav-link" href="#program">
                    MY PROGRAM
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#ask">
                    ASK DOUBT
                  </a>
                </li>
              </ul>
              <span className="navbar-nav">
                <li className="none">
                  <img
                    src="./images/cart.png"
                    alt=""
                    width="25"
                    className="d-inline block nav"
                  />
                </li>
                <li className="none">
                  <img
                    src="./images/bell.png"
                    alt=""
                    width="25"
                    className="d-inline block nav"
                  />
                </li>
                <li className="none dropdown">
                  <img src="./images/ME.jpg" alt="" className="navround" />
                  <a
                    className="d-inline block dropdown-toggle"
                    href="#"
                    id="dropdownMenu"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Jahnabi
                  </a>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenu">
                    <a className="dropdown-item" href="#">
                      Action
                    </a>
                    <a className="dropdown-item" href="#">
                      Another action
                    </a>
                    <a className="dropdown-item" href="#">
                      Something else here
                    </a>
                  </div>
                </li>
              </span>
            </div>
          </div>
        </nav>
      </>
    );
  }
}

export default Navbar;
