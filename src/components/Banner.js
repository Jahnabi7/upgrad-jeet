import React, { Component } from "react";

class Banner extends Component {
  render() {
    return (
      <>
        <div className="container banner">
          <div className="card text-white">
            <img src="./images/bg.jpg" className="card-img" alt="..." />
            <div className="card-img-overlay">
              <h2 className="card-title">
                Lorem ipsum dolor sit
                <br />
                amet, consectetur elit
              </h2>
              <p className="card-text">
                Lorem ipsum dolor sit amet, consectetur adipisicing
                <br />
                elit, sed do eiusmod tempor incididunt
              </p>
              <button type="button" className="btn">
                Explore our courses &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; →
              </button>
              <p className="card-text pt-3">Lorem ipsum dolor sit</p>
            </div>
          </div>
          <div className="container links">
            <div className="links">
              <a href="" className="one">
                Features
              </a>
              <span className="vl"></span>

              <a href="" className="one">
                Knowledge Center
              </a>
              <span className="vl"></span>

              <a href="" className="one">
                Resources
              </a>
              <span className="vl"></span>

              <a href="" className="one">
                Blogs
              </a>
              <span className="vl"></span>

              <a href="" className="one">
                2020 Answer Key
              </a>
            </div>
         
          </div>
        </div>
      </>
    );
  }
}

export default Banner;
