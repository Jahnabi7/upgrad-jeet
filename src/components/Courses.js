import React, { Component } from "react";

class Courses extends Component {
  render() {
    return (
      <div className="container course">
        <div className="container course text-center">
        <h2 className="color">Our Courses</h2>
        <p>
          Ex eiusmod aute quis dolore proident mollit laborum
          <br /> non sunt consectetur cupidatat officia ea sunt.
        </p>
        <div className="row course">
          <div className="col-xl-6 col-12">
            <div className="container container-box">
            <div className="box">
              <img src="./images/bg2.jpg" className="card-img-top" alt="..." />
              </div>
              <div className="box overlay">
                <p>
                  Adipisicing do sint nostrud ullamco irure pariatur Lorem
                  eiusmod aliqua esse ipsum sint culpa.
                </p>
                <a href="#" className="btn btn-sm">
                  KNOW MORE
                </a>
              </div>
              </div>
          </div>
          <div className="col-xl-6 col-12">
            <div className="container container-box">
            <div className="box">
              <img src="./images/bg2.jpg" className="card-img-top" alt="..." />
              </div>
              <div className="box overlay">
                <p>
                  Eiusmod ipsum ex aute duis cillum irure aliquip aute ad minim.
                  aliqua esse ipsum sint culpa.
                </p>
                <a href="#" className="btn btn-sm">
                  KNOW MORE
                </a>
              </div>
              </div>
          </div>
          <div className="col-xl-6 col-12">
            <div className="container container-box">
            <div className="box">
              <img src="./images/bg2.jpg" className="card-img-top" alt="..." />
              </div>
              <div className="box overlay">
                <p>
                  Occaecat laborum amet sit tempor commodo qui elit consequat.
                  aliqua esse ipsum sint culpa.
                </p>
                <a href="#" className="btn btn-sm">
                  KNOW MORE
                </a>
              </div>
              </div>
          </div>
          <div className="col-xl-6 col-12">
            <div className="container container-box">
            <div className="box">
              <img src="./images/bg2.jpg" className="card-img-top" alt="..." />
              </div>
              <div className="box overlay">
                <p>
                  Sint aliqua minim irure nulla ipsum nulla dolor amet nulla
                  officia mollit proident pariatur Lorem.
                </p>
                <a href="#" className="btn btn-sm">
                  KNOW MORE
                </a>
              </div>
              </div>
          </div>
        </div>
        </div>
      </div>
    );
  }
}

export default Courses;
