import React, { Component } from "react";

class Testimonials extends Component {
  render() {
    return (
      <div className="container testimonials">
        <div className="container text-center">
        <h2 className="color pt-5">Ranker Testimonials</h2>
        <p>
          Ipsum minim aliquip culpa nostrud irure duis.
          <br />
          Do nulla incididunt sit aliqua et ex.
        </p>
        <div className="row text-left">
          <div className="col-xl-4 col-lg-6 col-12">
            <div className="card m-2 two">
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-3 col-12">
                    <img src="./images/ME.jpg" alt="" className="round" />
                  </div>
                  <div className="col-lg-9 col-12">
                    <p><b>
                      Lorem ipsum dolor sit 
                      </b></p>
                    <p>
                      Amet aliqua incididunt voluptate non 
                    </p>
                  </div>
                  <div className="col-12">
                    Exercitation dolor pariatur mollit magna quis velit magna.
                    Ut ex anim ullamco non laboris amet aute. Id nostrud ad
                    pariatur ut qui magna. Exercitation ut ad ex duis dolore
                  </div>
                  <div className="col-6"><a className="link" href="">READ MORE</a></div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4 col-lg-6 col-12">
          <div className="card m-2 two">
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-3 col-12">
                    <img src="./images/ME.jpg" alt="" className="round" />
                  </div>
                  <div className="col-lg-9 col-12">
                    <p><b>
                      Lorem ipsum dolor sit 
                      </b></p>
                    <p>
                      Amet aliqua incididunt voluptate non 
                    </p>
                  </div>
                  <div className="col-12">
                    Exercitation dolor pariatur mollit magna quis velit magna.
                    Ut ex anim ullamco non laboris amet aute. Id nostrud ad
                    pariatur ut qui magna. Exercitation ut ad ex duis dolore
                  </div>
                  <div className="col-6"><a className="link" href="">READ MORE</a></div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4 col-lg-6 col-12">
          <div className="card m-2 two">
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-3 col-12">
                    <img src="./images/ME.jpg" alt="" className="round" />
                  </div>
                  <div className="col-lg-9 col-12">
                    <p><b>
                      Lorem ipsum dolor sit 
                      </b></p>
                    <p>
                      Amet aliqua incididunt voluptate non 
                    </p>
                  </div>
                  <div className="col-12">
                    Exercitation dolor pariatur mollit magna quis velit magna.
                    Ut ex anim ullamco non laboris amet aute. Id nostrud ad
                    pariatur ut qui magna. Exercitation ut ad ex duis dolore
                  </div>
                  <div className="col-6"><a className="link" href="">READ MORE</a></div>
                </div>
              </div>
            </div> 
          </div>
          <div className="col-xl-4 col-lg-6 col-12">
          <div className="card m-2 two">
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-3 col-12">
                    <img src="./images/ME.jpg" alt="" className="round" />
                  </div>
                  <div className="col-lg-9 col-12">
                    <p><b>
                      Lorem ipsum dolor sit 
                      </b></p>
                    <p>
                      Amet aliqua incididunt voluptate non 
                    </p>
                  </div>
                  <div className="col-12">
                    Exercitation dolor pariatur mollit magna quis velit magna.
                    Ut ex anim ullamco non laboris amet aute. Id nostrud ad
                    pariatur ut qui magna. Exercitation ut ad ex duis dolore
                  </div>
                  <div className="col-6"><a className="link" href="">READ MORE</a></div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4 col-lg-6 col-12">
          <div className="card m-2 two">
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-3 col-12">
                    <img src="./images/ME.jpg" alt="" className="round" />
                  </div>
                  <div className="col-lg-9 col-12">
                    <p><b>
                      Lorem ipsum dolor sit 
                      </b></p>
                    <p>
                      Amet aliqua incididunt voluptate non 
                    </p>
                  </div>
                  <div className="col-12">
                    Exercitation dolor pariatur mollit magna quis velit magna.
                    Ut ex anim ullamco non laboris amet aute. Id nostrud ad
                    pariatur ut qui magna. Exercitation ut ad ex duis dolore
                  </div>
                  <div className="col-6"><a className="link" href="">READ MORE</a></div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4 col-lg-6 col-12">
          <div className="card m-2 two">
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-3 col-12">
                    <img src="./images/ME.jpg" alt="" className="round" />
                  </div>
                  <div className="col-lg-9 col-12">
                    <p><b>
                      Lorem ipsum dolor sit 
                      </b></p>
                    <p>
                      Amet aliqua incididunt voluptate non 
                    </p>
                  </div>
                  <div className="col-12">
                    Exercitation dolor pariatur mollit magna quis velit magna.
                    Ut ex anim ullamco non laboris amet aute. Id nostrud ad
                    pariatur ut qui magna. Exercitation ut ad ex duis dolore
                  </div>
                  <div className="col-6"><a className="link" href="">READ MORE</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    );
  }
}

export default Testimonials;
